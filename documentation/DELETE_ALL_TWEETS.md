# Delete all tweets script

Script to delete all tweets of yours (only the last 3200 since the api is limited)

```
grrfe@feowo:~$ python3 delete_all_tweets.py --help
usage: delete_all_tweets.py [-h] [-t TOKEN_FOLDER] username

Delete all Tweets

positional arguments:
  username              Your Twitter username

optional arguments:
  -h, --help            show this help message and exit
  -t TOKEN_FOLDER, --token-folder TOKEN_FOLDER
                        Specifies a folder where the tokens are located;
                        default = %current_dir%/tokens

```

