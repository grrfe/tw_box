# Download status media

Download all pictures / videos / gifs from a given tweet

```
grrfe@feowo:~$ python3 download_status_media.py --help
usage: download_status_media.py [-h] [-t TOKEN_FOLDER] [-o OUTPUT_FOLDER]
                                username status_id

Twitter status media downloader

positional arguments:
  username              Your Twitter username
  status_id             The status id or url

optional arguments:
  -h, --help            show this help message and exit
  -t TOKEN_FOLDER, --token-folder TOKEN_FOLDER
                        Specifies a folder where the tokens are located;
                        default = %current_dir%/tokens
  -o OUTPUT_FOLDER, --output-folder OUTPUT_FOLDER
                        Specifies the output folder; default = %current_dir%
```