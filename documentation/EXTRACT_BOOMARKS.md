# Extract bookmarks script

Extracts all mobile twitter bookmarks

```
grrfe@feowo:~$ python3 extract_bookmarks.py --help
usage: extract_bookmarks.py [-h] [-p PASSWORD] [-t TOKEN_FOLDER]
                            [-f OUTPUT_FILE] [-d] [-df DOWNLOAD_FOLDER]
                            username

Extract your twitter bookmarks (from the mobile app)

positional arguments:
  username              Your Twitter username

optional arguments:
  -h, --help            show this help message and exit
  -p PASSWORD, --password PASSWORD
                        Your Twitter password (or a file containing the
                        password), only required when there is no token file /
                        the tokens are invalid / the username or password
                        changed
  -t TOKEN_FOLDER, --token-folder TOKEN_FOLDER
                        Specifies a folder where the tokens are located;
                        default = %current_dir%/tokens
  -f OUTPUT_FILE, --output-file OUTPUT_FILE
                        Specifies the file where the bookmarks will be
                        extracted to; default = extracted_bookmarks.txt
  -d, --download        Download all media bookmarks
  -df DOWNLOAD_FOLDER, --download-folder DOWNLOAD_FOLDER
                        Set the download directory; default = %current_dir%
```

