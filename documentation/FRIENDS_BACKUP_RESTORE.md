# Friend backup&restore script

Backup all friend of yours (people you follow) and restore them (for example if your old account got suspended)

```
grrfe@feowo:~$ python3 friends_backup_restore.py --help
usage: friends_backup_restore.py [-h] [-t TOKEN_FOLDER] (-b | -r) [-f FILE]
                                 username

Twitter Follower Backup/Restore

positional arguments:
  username              Your Twitter username

optional arguments:
  -h, --help            show this help message and exit
  -t TOKEN_FOLDER, --token-folder TOKEN_FOLDER
                        Specifies a folder where the tokens are located;
                        default = %current_dir%/tokens
  -b, --backup          Stores all users you are currently following to a file
                        called %YOUR_TWITTER_USERNAME%_following.json
  -r, --restore         Tries to follow every user listed in
                        %YOUR_TWITTER_USERNAME%_following.json
  -f FILE, --file FILE  Choose your own input/output file (use %datetime% to
                        include the current datetime in the filename)
```