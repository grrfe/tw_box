# Rip thread script

Download all media from a thread (input the last twitter status id / url to tweet)

```
grrfe@feowo:~$ python3 rip_thread.py --help
usage: rip_thread.py [-h] [-t TOKEN_FOLDER] [-o OUTPUT_FOLDER]
                     username status_id

Twitter media thread ripper

positional arguments:
  username              Your Twitter username
  status_id             The id or url of the last status in the thread

optional arguments:
  -h, --help            show this help message and exit
  -t TOKEN_FOLDER, --token-folder TOKEN_FOLDER
                        Specifies a folder where the tokens are located;
                        default = %current_dir%/tokens
  -o OUTPUT_FOLDER, --output-folder OUTPUT_FOLDER
                        Specifies the output folder; default =
                        %current_dir%/ouput/

```