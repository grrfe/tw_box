import argparse
import json

import tweepy

from account.session.account_handler import AccountHandler, LoginType
from account.session.twitter_api_session import TwitterAPISession

parser = argparse.ArgumentParser(description="Delete all tweets from GDRP archive")
parser.add_argument("file", help="GDPR tweets file (tweets.js) WITHOUT \"window.YTD.tweet.part0 =\" at the beginning")
parser.add_argument("-o", "--override-default-account", help="Username to use instead of the set default account")

args = parser.parse_args()

account_handler = AccountHandler()
tweepy_api = TwitterAPISession.make_tweepy(account_handler.get_sess(LoginType.API, args.override_default_account))
account_handler.save()

with open(args.file, "r", encoding="utf8") as file:
    tweets = json.load(file)
    for _tw in tweets:
        tweet = _tw["tweet"]
        id_str = tweet["id_str"]
        try:
            print(f"Deleting {id_str}")
            tweepy_api.destroy_status(id=id_str)
        except tweepy.error.TweepError as e:
            print(e)
