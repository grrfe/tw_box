#!/usr/bin/python3
from abc import ABC, abstractmethod

import tweepy

from account.session.account_handler import LoginType
from account.session.twitter_api_session import TwitterAPISession


class CMD(ABC):
    def __init__(self, name, parameter_str, req_params, description):
        self.name = name
        self.parameter_str = parameter_str
        self.req_params = req_params
        self.description = description

    @abstractmethod
    def parse(self, account_handler, parameters):
        pass

    def __str__(self):
        return self.name


class SwapDefault(CMD):

    def __init__(self):
        super(SwapDefault, self).__init__("swap-default", "<default_acc_num>", 1, "Swaps the default account")

    def parse(self, account_handler, parameters):
        num = int(parameters[0])
        acc = account_handler.accounts[num - 1]

        account_handler.default_account = acc
        account_handler.save()

        print(f"Default account has been set to {acc.username}")


class Login(CMD):
    def __init__(self):
        super(Login, self).__init__("login", "<username> [password]", 1, "Log into a new account")

    def parse(self, account_handler, parameters):
        if len(parameters) == 2:
            password = parameters[1]
        else:
            password = input("Input your password: ")

        account = account_handler.add_account(parameters[0], password, [LoginType.API], True)

        tweepy_api = TwitterAPISession.make_tweepy(account_handler.get_sess(LoginType.API))
        account.account_id = tweepy_api.get_user(screen_name=account.username).id_str

        account_handler.save()
        print("Account has been added and set to default!")


class Remove(CMD):
    def __init__(self):
        super(Remove, self).__init__("remove", "<num>", 1, "Remove an account from this list")

    def parse(self, account_handler, parameters):
        if len(parameters) == 1:
            num = int(parameters[0])
            acc = account_handler.accounts[num - 1]

            account_handler.remove_account(acc)
            if account_handler.default_account == acc:
                account_handler.default_account = None

            account_handler.save()

            print("Account has been removed")


class RefreshHandle(CMD):
    def __init__(self):
        super(RefreshHandle, self).__init__("refresh-handle", "<num>", 1,
                                            "Refresh the handle of the account in case you changed it")

    def parse(self, account_handler, parameters):
        if len(parameters) == 1:
            num = int(parameters[0])
            account = account_handler.accounts[num - 1]

            tweepy_api = TwitterAPISession.make_tweepy(account_handler.get_sess(LoginType.API, account))
            account.username = tweepy_api.get_user(user_id=account.account_id).screen_name

            account_handler.save()

            print(f"Handle has been refreshed (new handle {account.username})")
