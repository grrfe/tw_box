import webbrowser
from typing import Tuple, Dict, TypedDict

import tweepy


class AccessTokens(TypedDict):
    oauth_token: str
    oauth_token_secret: str


class TwitterAPISession:
    __CONSUMER_KEY = "3rJOl1ODzm9yZy63FACdg"
    __CONSUMER_SECRET = "5jPoQ5kQvMJFDYRNE8bQ4rHuds4xJqhvgNJM4awaE8"

    __EXT = "twitter_tokens"

    def __init__(self, username: str, password: str, access_tokens: AccessTokens):
        self.username = username
        self.password = password
        self.access_tokens = access_tokens

    @staticmethod
    def get_file(username: str) -> str:
        return f"{username}.{TwitterAPISession.__EXT}"

    def create_auth(self):
        if self.access_tokens is not None:
            auth = self.__get_auth_handler()
            auth.set_access_token(self.access_tokens["oauth_token"], self.access_tokens["oauth_token_secret"])

            return auth
        else:
            auth = self.authorize_app()
            self.access_tokens = {"oauth_token": auth.access_token,
                                  "oauth_token_secret": auth.access_token_secret}

            return auth

    def authorize_app(self) -> tweepy.OAuth1UserHandler:
        auth = self.__get_auth_handler()
        url = auth.get_authorization_url()
        print(f"auth_url=({url})")

        webbrowser.open(url)
        auth.get_access_token(input("Input the PIN from the opened webpage: "))

        return auth

    @staticmethod
    def make_tweepy(tw_api_sess):
        return tweepy.API(auth=tw_api_sess.create_auth(), wait_on_rate_limit=True)

    @staticmethod
    def __get_auth_handler() -> tweepy.OAuth1UserHandler:
        return tweepy.OAuth1UserHandler(consumer_key=TwitterAPISession.__CONSUMER_KEY,
                                        consumer_secret=TwitterAPISession.__CONSUMER_SECRET)
