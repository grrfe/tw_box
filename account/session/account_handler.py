import json
import os
from enum import Enum
from pathlib import Path

from account.session.twitter_api_session import TwitterAPISession


class LoginType(Enum):
    API = TwitterAPISession


class Account:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.account_id = None
        self.session = {}

    def to_json(self):
        _json = {"username": self.username, "password": self.password, "account_id": self.account_id, "tokens": []}

        for k, v in self.session.items():
            _json["tokens"].append(
                {
                    "type": k,
                    "data": v.access_tokens
                }
            )

        return _json

    def login(self, login_type):
        return self.load(login_type, None)

    def load(self, login_type, data):
        sess = login_type.value(self.username, self.password, data)
        self.session[login_type.name] = sess
        return sess


class AccountHandler:
    __USER_HOME = str(Path.home())
    __TW_BOX_DIR = f"{__USER_HOME}/.tw_box"
    __ACCOUNTS = f"{__TW_BOX_DIR}/accounts.json"

    def __init__(self):
        self.accounts = []
        self.default_account = None

        if os.path.isfile(AccountHandler.__ACCOUNTS):
            with open(AccountHandler.__ACCOUNTS, "r") as file:
                json_file = json.load(file)

                for json_account in json_file["accounts"]:
                    account = Account(json_account["username"], json_account["password"])
                    account.account_id = json_account["account_id"]
                    if account.username == json_file["default"]:
                        self.default_account = account

                    for token in json_account["tokens"]:
                        login_type = LoginType[token["type"]]
                        data = token["data"]

                        account.load(login_type, data)

                    self.accounts.append(account)

    def remove_account(self, account):
        self.accounts.remove(account)

    def add_account(self, username, password, types, default_acc):
        account = self.exists_account(username)
        if account is None:
            account = Account(username, password)

        for api in types:
            account.login(api)

        if self.default_account is None or default_acc:
            self.default_account = account

        self.accounts.append(account)

        return account

    def exists_account(self, username):
        for account in self.accounts:
            if account.username == username:
                return account

    def save(self):
        account_output = []
        if self.default_account is None:
            self.default_account = self.accounts[0]

        with open(AccountHandler.__ACCOUNTS, "w") as file:
            for account in self.accounts:
                account_output.append(account.to_json())

            json.dump({"default": self.default_account.username, "accounts": account_output}, file, indent=4)

    def get_sess(self, _type, account_override=None):
        if account_override is not None:
            account = self.exists_account(account_override)
            if account is not None:
                return self.find_sess(account, _type)

        if self.default_account is None:
            return None

        return self.find_sess(self.default_account, _type)

    @staticmethod
    def find_sess(account, _type):
        for k, v in account.session.items():
            if k == _type.name:
                return v
