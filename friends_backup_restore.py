#!/usr/bin/python3
import tweepy
import json
import argparse
import time
import os
import datetime

import tweepy.errors

from account.session.account_handler import AccountHandler, LoginType
from account.session.twitter_api_session import TwitterAPISession

parser = argparse.ArgumentParser(description="Twitter Follower Backup/Restore")
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument("-b", "--backup", help="Stores all users you are currently following to a file called "
                                          "%%YOUR_TWITTER_USERNAME%%_following.json", action="store_true")
group.add_argument("-r", "--restore",
                   help="Tries to follow every user listed in %%YOUR_TWITTER_USERNAME%%_following.json",
                   action="store_true")
parser.add_argument("-f", "--file", help="Choose your own input/output file (use %%datetime%% to include the "
                                         "current datetime in the filename)")
parser.add_argument("-o", "--override-default-account", help="Username to use instead of the set default account")

args = parser.parse_args()


def find_following():
    _following = {}

    for user in tweepy.Cursor(tweepy_api.get_friends, count=5000).items():
        print("@{} (id: {})".format(user.screen_name, user.id_str))
        _following[user.id_str] = user.screen_name

    return _following


def get_friends():
    _friends = {}
    for friend in tweepy.Cursor(tweepy_api.get_friends, count=100).items():
        _friends[friend.id_str] = friend.screen_name

    return _friends


def follow_user(_id, _username):
    try:
        user = tweepy_api.create_friendship(user_id=_id)
        if user is not None:
            print("Followed {}".format(_username))
        else:
            print("Couldn't follow {}".format(_username))
    except tweepy.errors.HTTPException as exc:
        if 158 in exc.api_errors:
            print("You can't follow yourself")
        elif 108 in exc.api_errors:
            print("Couldn't find account {}".format(username))
        elif 162 in exc.api_errors:
            print("{} blocked you, skipping..".format(username))
        elif 160 not in exc.api_errors:
            print("Rate limited while trying to follow {}, going to sleep..".format(username))

            time.sleep(60)

            print("Retrying to follow {}".format(username))
            follow_user(_id, username)


account_handler = AccountHandler()
tw = account_handler.get_sess(LoginType.API, args.override_default_account)
tweepy_api = TwitterAPISession.make_tweepy(tw)
account_handler.save()

if args.file is not None:
    args_folder = args.file[:args.file.rfind("/")]
    if not os.path.exists(args_folder):
        os.makedirs(args_folder)

    json_file = args.file
    if "%datetime%" in json_file:
        json_file = json_file.replace("%datetime%", str(datetime.datetime.now()))

    if "%username%" in json_file:
        json_file = json_file.replace("%username%", tw.username)
else:
    friends_dir = "friends"
    if not os.path.exists(friends_dir):
        os.makedirs(friends_dir)

    json_file = friends_dir + "/{}_following.json".format(tw.username)

if args.backup:
    print("Found friends: ")
    following = find_following()
    json.dump(following, open(json_file, "w+"), indent=4)

elif args.restore:
    following = json.load(open(json_file))
    print("Loaded {} accounts from {}".format(len(following), json_file))

    friends = get_friends()
    print("Currently following {} accounts".format(len(friends)))
    for _id, username in friends.items():
        print("Already following {} (id: {})".format(username, _id))

        if _id in following:
            print("Removing from list of accounts to follow since account is already being followed")
            del following[_id]

    print("Trying to follow {} accounts..".format(len(following)))

    for _id, username in following.items():
        follow_user(_id, username)
