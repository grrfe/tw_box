#!/usr/bin/python3
import argparse
import os
import re

from account.session.account_handler import AccountHandler, LoginType
from account.session.twitter_api_session import TwitterAPISession
from util.status_media_util import find_media, download_media

parser = argparse.ArgumentParser(description="Twitter status media downloader")
parser.add_argument("status_id", help="The status id or url")
parser.add_argument("-o", "--output-folder", help="Specifies the output folder; default = %%current_dir%%",
                    default=os.getcwd())
parser.add_argument("-a", "--override-default-account", help="Username to use instead of the set default account")
parser.add_argument("-f", "--override-file-name", help="Override filename")

args = parser.parse_args()

account_handler = AccountHandler()
tweepy_api = TwitterAPISession.make_tweepy(account_handler.get_sess(LoginType.API, args.override_default_account))
account_handler.save()

if not os.path.exists(args.output_folder):
    os.makedirs(args.output_folder)

regex = re.compile(".*\.com\/.*\/status\/(\d+)")
regex_match = regex.match(args.status_id)
if regex_match:
    args.status_id = regex_match.group(1)

for i, media in enumerate(find_media(tweepy_api, args.status_id)):
    print("-> Found media {}".format(media))
    download_media(i, media, args.status_id, args.output_folder, args.override_file_name)
