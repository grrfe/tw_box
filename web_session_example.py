from account.session.twitter_web_session import TwitterWebSession

#file with first line username, second line password
f = open(".ignored/user_pass", "r")

username = f.readline().rstrip()
password = f.readline().rstrip()
print(username)

tw = TwitterWebSession(username, "__nopush/tokens")
if tw.is_new_session():
    print("new session")
    session = tw.create_new_session(username, password)
else:
    print("existing session")
    session = tw.create_session()

# do something with requests.Session object (send query to twitter,
# already contains all necessary headers and cookies for an authenticated request)
