import json
import os

import requests
from tqdm import tqdm


def find_media(api, status):
    # print(json.dumps(api.get_status(status, tweet_mode="extended")._json))
    return parse_status_media_json(api.get_status(status, include_entities=True, tweet_mode="extended")._json)


def parse_status_media_json(_json):
    media = []
    if "extended_entities" in _json:
        for item in _json["extended_entities"]["media"]:
            if item["type"] == "video" or item["type"] == "animated_gif":
                max_bitrate_variant = None
                for variant in item["video_info"]["variants"]:
                    if "bitrate" in variant:
                        if max_bitrate_variant is None:
                            max_bitrate_variant = variant
                        elif variant["bitrate"] > max_bitrate_variant["bitrate"]:
                            max_bitrate_variant = variant

                media.append(max_bitrate_variant["url"])
            else:
                media.append(item["media_url_https"])
    return media


def download_media(index, url, status, folder, custom_file_name=None):
    if not os.path.exists(folder):
        os.makedirs(folder)

    get_param_idx = url.find("?")
    if get_param_idx != -1:
        url = url[:get_param_idx]

    last_slash = url.rfind("/")
    last_dot = url.rfind(".")
    url_file_name = url[last_slash + 1:last_dot]
    url_ext = url[last_dot + 1:]

    dl_file = f"{status}_"
    if index is not None:
        dl_file += f"{index}_"

    if custom_file_name is not None:
        dl_file += custom_file_name
    else:
        dl_file += url_file_name

    dl_file += f".{url_ext}"


    download_path = os.path.join(folder, dl_file)

    resp = requests.get(url, stream=True)
    total = int(resp.headers.get('content-length', 0))
    with open(download_path, "wb") as file, tqdm(
            desc=download_path,
            total=total,
            unit="iB",
            unit_scale=True,
            unit_divisor=1024
    ) as bar:
        for data in resp.iter_content(chunk_size=1024):
            size = file.write(data)
            bar.update(size)
