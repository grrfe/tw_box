#!/usr/bin/python3
import tweepy
import os
import argparse
import re

from account.session.account_handler import AccountHandler, LoginType
from account.session.twitter_api_session import TwitterAPISession
from util.status_media_util import find_media, download_media

parser = argparse.ArgumentParser(description="Twitter media thread ripper")
parser.add_argument("status_id", help="The id or url of the last status in the thread")
parser.add_argument("-o", "--output-folder",
                    help="Specifies the output folder; default = %%current_dir%%/ripped_thread/",
                    default=os.getcwd() + "/ripped_thread/")
parser.add_argument("-a", "--override-default-account", help="Username to use instead of the set default account")

args = parser.parse_args()


def get_status(status, folder):
    json = tweepy_api.get_status(status, tweet_mode="extended")._json
    reply_to_id = json["in_reply_to_status_id_str"]

    print("Found status {}".format(status))
    for m in find_media(tweepy_api, status):
        print("-> Found media {}".format(m))
        download_media(None, m, status, folder)

    if reply_to_id is not None:
        get_status(reply_to_id, folder)


account_handler = AccountHandler()
tweepy_api = TwitterAPISession.make_tweepy(account_handler.get_sess(LoginType.API, args.override_default_account))
account_handler.save()

if not os.path.exists(args.output_folder):
    os.makedirs(args.output_folder)

regex = re.compile(".*\.com\/.*\/status\/(\d+)")
regex_match = regex.match(args.status_id)
if regex_match:
    args.status_id = regex_match.group(1)

get_status(args.status_id, args.output_folder)
