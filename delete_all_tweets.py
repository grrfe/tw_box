#!/usr/bin/python3
import tweepy
import argparse

from account.session.account_handler import LoginType, AccountHandler
from account.session.twitter_api_session import TwitterAPISession

parser = argparse.ArgumentParser(description="Delete all Tweets")
parser.add_argument("username", help="Your Twitter username")
parser.add_argument("-t", "--token-folder", help="Specifies a folder where the tokens are located; "
                                                 "default = %%current_dir%%/tokens", default="tokens")
parser.add_argument("-a", "--override-default-account", help="Username to use instead of the set default account")

args = parser.parse_args()

account_handler = AccountHandler()
tweepy_api = TwitterAPISession.make_tweepy(account_handler.get_sess(LoginType.API, args.override_default_account))
account_handler.save()

for tweet in tweepy.Cursor(tweepy_api.user_timeline, user_id=tweepy_api.me().id_str, tweet_mode="extended").items():
    print("Found tweet {}, destroying..".format(tweet.id_str))
    tweepy_api.destroy_status(id=tweet.id_str)
