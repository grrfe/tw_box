# tw_box

Collection of python3 twitter scripts

* [Delete all tweets](documentation/DELETE_ALL_TWEETS.md)
* [Download a all media from a tweet](documentation/DOWNLOAD_STATUS_MEDIA.md)
* [Download all media from a thread](documentation/RIP_TRHEAD.md)
* [Backup and restore friends](documentation/FRIENDS_BACKUP_RESTORE.md)
* [Extract bookmarks from the mobile twitter app](documentation/EXTRACT_BOOMARKS.md)

***Requirements***

`Python3`, `Pip (3)`

Please make sure you can run `python3` and `pip` / `pip3` from a terminal / powershell / cmd

***Dependencies***

* Tweepy: `pip3 install tweepy`

(Open a terminal/powershell/cmd etc. and type the command above)

***Usage***

* Download this project using either the download button or `Git` (`git clone https://gitlab.com/grrfe/tw_box.git`)
* Make sure that you fulfil the requirements
* Install the dependencies
* Open a terminal in the `tw_box` folder
* Type `python3 %NAME_OF_SCRIPT%.py`

