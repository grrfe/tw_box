#!/usr/bin/python3
import os
from pathlib import Path

from account.cmd import SwapDefault, Login, Remove, RefreshHandle
from account.session.account_handler import AccountHandler

__CMD = [SwapDefault(), Login(), Remove(), RefreshHandle()]


def find_command(name):
    for _cmd in __CMD:
        if _cmd.name == name:
            return _cmd


def list_accounts():
    print("Currently added: ")

    for idx in range(0, len(account_handler.accounts)):
        print(f"{idx + 1} {account_handler.accounts[idx].username}")


user_home = str(Path.home())
tw_box_dir = f"{user_home}/.tw_box"

if not os.path.exists(tw_box_dir):
    os.makedirs(tw_box_dir)

account_handler = AccountHandler()
list_accounts()

default_acc = account_handler.default_account
print("Default: " + (default_acc.username if default_acc is not None else "No default account"))
print("Available commands, exit using quit: ")
print(f"    list")
for cmd_input in __CMD:
    print(f"    {cmd_input.name} {cmd_input.parameter_str}")

while True:
    cmd_input = input("Input command: ")
    if cmd_input in ["quit", "exit", "stop"]:
        break

    cmd_arr = cmd_input.split(" ")
    if len(cmd_arr) >= 1:
        if cmd_arr[0] == "list":
            list_accounts()
        else:
            cmd = find_command(cmd_arr[0])

            if cmd is not None:
                if len(cmd_arr) - 1 >= cmd.req_params:
                    cmd.parse(account_handler, cmd_arr[1:])
                else:
                    print(f"Invalid input: {cmd.name} {cmd.parameter_str}")
            else:
                print("Invalid command!")
    else:
        print("Invalid input")
